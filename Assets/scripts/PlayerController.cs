﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	//floats
	public float speed;
	[SerializeField]
	private float tempoAntesDestruicao = 3.0f;
	public float jumpForce;
	public float intervaloAtaque;
	private float proximoAtaque;
    public float direcao;
	public float lives = 3;
	public float menosUmCoracao = 2;
    
	//booleans
	public static bool noChao = false;
	private bool facingRight = true;
	private bool jump = false;

	//other
	
	Animator animator;
	private Transform groundCheck;
	private Rigidbody2D rb;
	Collider2D[] colliders;
    private IEnumerator coroutine;


	void Start () {
		rb = gameObject.GetComponent<Rigidbody2D>();
		direcao = Input.GetAxisRaw("Horizontal");
		groundCheck = gameObject.transform.Find("GroundCheck");
		animator = gameObject.GetComponent<Animator>();
		
	}
	
	// Update is called once per frame
	void Update () {
        noChao = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        if(Input.GetButtonDown("Horizontal")){
			rb.velocity = new Vector2 (speed*direcao , rb.velocity.y);
			Debug.Log("velocidade" + rb.velocity);
		}

        if(Input.GetButtonDown("Jump") && noChao){
			Jump();
		}

		if (Input.GetKeyDown(KeyCode.X)){
			StartCoroutine(Atacando());
		}
	}
	void FixedUpdate(){
		float direcao = Input.GetAxisRaw("Horizontal");
		animator.SetFloat("Walking", Mathf.Abs(direcao));
		//mantem a velocidade do personagem constante
		rb.velocity = new Vector2(direcao*speed,rb.velocity.y);

		if(direcao > 0 && !facingRight){
			Flip();
		} else if(direcao < 0 && facingRight){
			Flip();
		}
		if(jump){
			rb.AddForce(new Vector2(0,jumpForce));
			jump = false;
            animator.SetBool("Grounded", false);
        }

	}

	void Flip(){
		facingRight = !facingRight;
		//inverte o valor da coordenada x no vetor
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	IEnumerator Atacando(){
		animator.SetTrigger("Ataque");
		proximoAtaque = Time.time + intervaloAtaque;
		colliders = new Collider2D[3];
		Debug.Log("colliders" + colliders);
		transform.Find("AreaArranhao").gameObject.GetComponent<Collider2D>()
			.OverlapCollider(new ContactFilter2D(), colliders);
		for(int i = 0; i < colliders.Length; i++){
			Debug.Log("col de i" + colliders[i]);
			if(colliders[i]!=null && colliders[i].gameObject.CompareTag("Enemy")){
				yield return new WaitForSeconds(tempoAntesDestruicao);
					Destroy(colliders[i].gameObject);
			
			}
		}
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "Ground")
        {
            noChao = true;
            animator.SetBool("Grounded", true) ;
        }

		if(col.gameObject.tag == "Enemy" && !Input.GetKey(KeyCode.X)){
			animator.SetTrigger("Morreu");
			//Destroy(gameObject);
		}
    }

    void Jump(){
		rb.AddForce(new Vector2(0,jumpForce));
		animator.SetTrigger("Pulou");
		jump = false;
	}
	
    

}