﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
	public float velocidade;
	public bool direcao;
	
	public float duracaoDirecao;
	private float tempoNaDirecao;
	private Animator animator; 


	public float intervaloAtaque;
	private float contagemIntervalo;
	private bool atacou;
	public float distanciaAtaque;
	
	public GameObject ataque;



	

	// Use this for initialization
	void Start () {
		animator = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if (direcao) {
			transform.eulerAngles = new Vector2(0, 0);
		} else {
			transform.eulerAngles = new Vector2(0, 180);
		}
		transform.Translate(Vector2.right * velocidade * Time.deltaTime);

		tempoNaDirecao += Time.deltaTime;
		if (tempoNaDirecao >= duracaoDirecao) {
			tempoNaDirecao = 0;
			direcao = !direcao;
		}
		if (atacou) {
 			contagemIntervalo += Time.deltaTime;
 			if (contagemIntervalo >= intervaloAtaque) {
				atacou = false;
				contagemIntervalo = 0;
			}
		}
	}


	void OnCollisionEnter2D(Collision2D other) {
	if (other.gameObject.tag == "Player" && !Input.GetKey(KeyCode.X)) {
		animator.SetTrigger("Atacou");
		Destroy(other.gameObject);
		//player.PerdeVida(30);
		other.gameObject.transform.Translate(-Vector2.right);
	}
	}
}
