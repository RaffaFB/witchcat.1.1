using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	//floats
	[SerializeField]public float speed = 3.5f;
    [SerializeField] public float jumpForce = 400f;
    [SerializeField] public float intervaloAtaque;
    [SerializeField] private float proximoAtaque;
    public float direcao;
    
	//booleans
	public static bool noChao = false;
    [SerializeField] private bool facingRight = true;
    [SerializeField] private bool jump = false;

	//other
	
	Animator animator;
	
	private Rigidbody2D rb;
    

	void Start () {
		rb = gameObject.GetComponent<Rigidbody2D>();
		direcao = Input.GetAxisRaw("Horizontal");
		
		animator = gameObject.GetComponent<Animator>();
        
	}
	
	// Update is called once per frame
	void Update () {
        
        if(Input.GetButtonDown("Horizontal")  ){
			rb.velocity = new Vector2 (speed*direcao , rb.velocity.y);
			Debug.Log("velocidade" + rb.velocity);
		}

        if(Input.GetButtonDown("Jump") && noChao){
			Jump();
		}

		if (Input.GetKeyDown(KeyCode.X) && Time.time > proximoAtaque){
			Atacando();
		}
	}
	void FixedUpdate(){
		float direcao = Input.GetAxisRaw("Horizontal");
		animator.SetFloat("Walking", Mathf.Abs(direcao));
		//mantem a velocidade do personagem constante
		rb.velocity = new Vector2(direcao*speed,rb.velocity.y);

		if(direcao > 0 && !facingRight){
			Flip();
		} else if(direcao < 0 && facingRight){
			Flip();
		}
		if(jump){
			rb.AddForce(new Vector2(0,jumpForce));
			jump = false;
            animator.SetBool("Grounded", false);
        }

	}

	void Flip(){
		facingRight = !facingRight;
		//inverte o valor da coordenada x no vetor
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	void Atacando(){
		animator.SetTrigger("Attack");
		proximoAtaque = Time.time + intervaloAtaque;
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "Ground")
        {
            noChao = true;
            animator.SetBool("Grounded", true) ;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.gameObject.tag =="Ground")
        {
            noChao = false;
            animator.SetBool("Grounded", false);
        }
    }

    void Jump(){
		rb.AddForce(new Vector2(0,jumpForce));
		animator.SetTrigger("Jump");
		jump = false;
	}
    

}
